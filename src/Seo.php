<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 6-7-18
 * Time: 11:36
 */

namespace Galatea\Seo;

class Seo
{

    /**
     * @var MetaTags
     */
    private $metaTags;

    private $title;

    /**
     * Seo constructor.
     *
     * @param MetaTags $metaTags
     */
    public function __construct(MetaTags $metaTags = null)
    {
        if (!$metaTags) {
            $this->metaTags = new MetaTags([]);
        } else {
            $this->metaTags = $metaTags;
        }
    }

    public function getMetaTags() {
        return $this->metaTags;
    }

    public function append($value)
    {
       $this->metaTags->offsetSet($value->getName(), $value);
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    public function getTitle() {
        return $this->title;
    }
}
