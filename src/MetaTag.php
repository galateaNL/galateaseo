<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 6-7-18
 * Time: 11:36
 */

namespace Galatea\Seo;

class MetaTag
{
    protected $name = '';
    protected $content = '';
    protected $property = '';


    protected $encoding = 'UTF-8';

    /**
     * MetaTag constructor.
     *
     * @param string $name
     * @param string $content
     * @param string $property
     */
    public function __construct(string $name, string $content, string $property=null)
    {
        $this->name = $name;
        $this->content = $content;
        $this->property = $property;
    }

    /**
     * @return string
     */
    public function getEncoding(): string
    {
        return $this->encoding;
    }

    /**
     * @param string $encoding
     */
    public function setEncoding(string $encoding): void
    {
        $this->encoding = $encoding;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function toHtml()
    {
        if (strlen($this->name) > 0) {
            return sprintf(
                '<meta name="%s" content="%s"/>',
                $this->encodeAttribute($this->name),
                $this->encodeAttribute($this->content)
            );
        } elseif (strlen($this->property) > 0) {
            return sprintf(
                '<meta property="%s" content="%s"/>',
                $this->encodeAttribute($this->name),
                $this->encodeAttribute($this->content)
            );
        } else {
            throw new \Exception("Metatag should have content or property");
        }
    }

    protected function encodeAttribute(string $string)
    {
        return htmlspecialchars($string, ENT_QUOTES, $this->encoding, true);
    }

    /**
     * @return string
     */
    public function getProperty(): string
    {
        return $this->property;
    }

    /**
     * @param string $property
     */
    public function setProperty(string $property): void
    {
        $this->property = $property;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }
}
