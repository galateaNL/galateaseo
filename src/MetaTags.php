<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 6-7-18
 * Time: 12:01
 */

namespace Galatea\Seo;

class MetaTags extends \ArrayObject
{
    /**
     * @param mixed $index
     *
     * @return MetaTag
     */
    public function offsetGet($index): MetaTag
    {
        return parent::offsetGet($index);
    }

    /**
     * @param mixed $index
     * @param MetaTags $newval
     */
    public function offsetSet($index, $newval)
    {
        parent::offsetSet($index, $newval);
    }
}
